use std::path::PathBuf;

use clap::Parser;

/// Merges ghost map and real map tiles into a merged map
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// The folder in which to find the ghost map.
    pub ghost_map: PathBuf,

    /// The folder in which to find the real map.
    pub real_map: PathBuf,

    /// The output folder in which to put the merged map.
    pub merged_map: PathBuf,

    /// Whether to blur the ghost image.
    #[clap(long)]
    pub blur: bool,
}
