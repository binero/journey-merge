use anyhow::{Context, Result};
use args::Args;
use clap::Parser;
use image::{
    buffer::ConvertBuffer, io::Reader as ImageReader, DynamicImage, GrayAlphaImage, ImageError,
    Rgba, RgbaImage,
};
use rayon::iter::{IntoParallelIterator, ParallelIterator};
use std::{fs, path::Path};

mod args;

fn main() -> Result<()> {
    let args = Args::parse();

    process_maps(&args.ghost_map, &args.real_map, &args.merged_map, args.blur)?;

    Ok(())
}

fn dir_entry_is_image(entry: &fs::DirEntry) -> bool {
    let path = entry.path();

    path.is_file() && path.extension().map(|ext| ext == "png").unwrap_or(false)
}

fn read_image(path: &impl AsRef<Path>) -> Result<DynamicImage> {
    ImageReader::open(path.as_ref())
        .with_context(|| format!("Failed to open image at path: {}", path.as_ref().display()))?
        .decode()
        .with_context(|| {
            format!(
                "Failed to decode image at path: {}",
                path.as_ref().display()
            )
        })
}

fn process_maps(
    ghost_map_folder: &impl AsRef<Path>,
    real_map_folder: &impl AsRef<Path>,
    merged_map_folder: &impl AsRef<Path>,
    should_blur: bool,
) -> Result<()> {
    let ghost_map_folder = ghost_map_folder.as_ref();
    let real_map_folder = real_map_folder.as_ref();
    let merged_map_folder = merged_map_folder.as_ref();

    // Create the merged map folder if it doesn't exist
    if !merged_map_folder.exists() {
        fs::create_dir_all(merged_map_folder).with_context(|| {
            format!(
                "Failed to create merged map folder at path: {}",
                merged_map_folder.display()
            )
        })?;
    }

    let dir_entries = fs::read_dir(ghost_map_folder)
        .with_context(|| {
            format!(
                "Failed to read ghost map folder at path: {}",
                ghost_map_folder.display()
            )
        })?
        .filter_map(Result::ok)
        .filter(dir_entry_is_image)
        .collect::<Vec<_>>();

    eprintln!(
        "Processing {} images in folder: {}",
        dir_entries.len(),
        ghost_map_folder.display()
    );

    dir_entries.into_par_iter().try_for_each(|entry| {
        let path = entry.path();
        let file_name = path.file_name().unwrap().to_str().unwrap().to_string();
        let ghost_image = read_image(&path)?.into_luma_alpha8();

        let ghost_image = if should_blur {
            blur_image(ghost_image)
        } else {
            ghost_image
        };

        let real_image_path = Path::new(real_map_folder).join(&file_name);
        let merged_image = if real_image_path.exists() {
            merge_images(ghost_image, read_image(&real_image_path)?.into_rgba8())?
        } else {
            ghost_image.convert()
        };

        let merged_image_path = Path::new(merged_map_folder).join(file_name);
        merged_image.save(&merged_image_path).with_context(|| {
            format!(
                "Failed to save merged image at path: {}",
                merged_image_path.display()
            )
        })?;

        Ok(())
    })
}

fn blur_image(mut ghost_image: GrayAlphaImage) -> GrayAlphaImage {
    // Merge every 4x4 square of pixels into a single pixel. We do this
    // in-place.

    let (width, height) = ghost_image.dimensions();

    for y in (0..height).step_by(8) {
        for x in (0..width).step_by(8) {
            let (count, average) = (0..8)
                .flat_map(|y_offset| {
                    let ghost_image = &ghost_image;
                    (0..8).map(move |x_offset| ghost_image.get_pixel(x + x_offset, y + y_offset))
                })
                .filter(|pixel| pixel.0[1] != 0)
                .map(|pixel| pixel.0[0])
                .fold((0, 0), |(count, sum), intensity| {
                    (count + 1, sum + intensity as u16)
                });

            let average = if count > 0 {
                (average / count) as u8
            } else {
                0
            };

            for y_offset in 0..8 {
                for x_offset in 0..8 {
                    let pixel = ghost_image.get_pixel_mut(x + x_offset, y + y_offset);
                    if pixel.0[1] != 0 {
                        pixel.0[0] = average;
                    }
                }
            }
        }
    }

    ghost_image
}

fn merge_images(
    ghost_image: GrayAlphaImage,
    real_image: RgbaImage,
) -> Result<RgbaImage, ImageError> {
    let (width, height) = ghost_image.dimensions();
    let mut result_image = RgbaImage::new(width, height);

    for (x, y, ghost_pixel) in ghost_image.enumerate_pixels() {
        let real_pixel = real_image.get_pixel(x, y);

        if real_pixel.0[3] != 0 {
            result_image.put_pixel(x, y, *real_pixel);
        } else {
            let intensity = ghost_pixel.0[0];
            let alpha = ghost_pixel.0[1];
            let new_pixel = Rgba([intensity, intensity, intensity, alpha]);
            result_image.put_pixel(x, y, new_pixel);
        }
    }

    Ok(result_image)
}
